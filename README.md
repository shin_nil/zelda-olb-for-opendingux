The Legend of Zelda: Oni Link Begins (OpenDingux Version [A320/GCW0])
=====================================================================
![Title Screen](http://2.bp.blogspot.com/-llugc3qCxlo/THcGyoyRa9I/AAAAAAAAADQ/Dx7ZnNenqRU/s1600/s1.JPG)

This is the second game of a trilogy developed by fans. My intention is to build all the three for OpenDingux.

Original code: [Vincent Jouillat](http://www.zeldaroth.fr)
--------------

History / Changelog
-------------------
###(29/08/2013)
* GCW-Zero alpha release

###(09/07/2012)

* OpenDingux release

###(26/08/2010)

* First release

Controls
--------

###Menus

* Move the pointer - D-Pad;
* Confirm - Start;
* Quit the game - Select.

###Game Play

* Move Link - D-Pad
* Run - Hold A (if you own boots)
* Use the sword - B (if you own a sword)
* Great technique - B to load, release when fully charged (after the tone) to use a spin attack.
* Look around - R + D-Pad (outside dungeons)
* Use selected object - Y (only when the selected object is useable)
* Use gloves - A
* See the map - X (outside or in a dungeon)
* See defeated monsters - L (after finding it)
* Teleportation - R (in a dungeon as Oni Link)
* Open a chest/Read/Speak - B
* Select/Confirm/Pass - Start
* Access/Quit the selection item menu - Start
* Quit/Save - Select


For more information about the game, including Walkthroughs, visit the creators: [http://www.zeldaroth.fr/us/index.php](http://www.zeldaroth.fr/us/index.php)  
For more information about the OpenDingux port, visit my page (portuguese): [http://www.shinnil.blogspot.com.br](http://www.shinnil.blogspot.com.br)  
For more information about OpenDingux (A320), visit [http://www.treewalker.org/opendingux/](http://www.treewalker.org/opendingux/)  
For more information about GCW-Zero, visit [http://www.gcw-zero.com/](http://www.gcw-zero.com/) 

